
\subsection{Tor's role in the research community}

Just about every major computer security conference these days has a paper
analyzing, attacking, or improving Tor. While fifteen years ago the field of
anonymous communications was mostly theoretical, with researchers speculating
that a given design should or shouldn't work, Tor now provides an actual
deployed testbed. Tor has become the gold standard for anonymous communications
research for three main reasons:

First, Tor's source code and specifications are open. Beyond its original
design document~\cite{tor-design}, Tor provides a clear and published set
of technical specifications~\cite{tor-specs} describing exactly how it
is built, why we made each design decision, and what security properties
it aims to offer. The Tor developers conduct design discussion in the
open, on public development mailing lists, and the public development
proposal process~\cite{tor-proposals} provides a clear path by which
other researchers can participate.

Second, Tor provides open APIs and maintains a set of tools to
help researchers and developers interact with the Tor software. The
Tor software's ``control port''~\cite{control-spec} lets controller
programs view and change configuration and status information, as well
as influence path selection. We provide easy instructions for setting
up separate private Tor networks for testing~\cite{jansen2012shadow}.
This modularity makes Tor
more accessible to researchers because they can run their own experiments
using Tor without needing to modify the Tor program itself.

Third, real users rely on Tor. Every day millions of people connect to the
Tor network and depend on it for a broad variety of security goals. In
addition to its emphasis on research and design, the Tor Project has
developed a reputation as a non-profit that fosters this community and
puts its users first. This real-world relevance motivates researchers
to help make sure Tor provides provably good security properties.

Tor attracts researchers precisely because it brings in so many
problems that are hard to solve yet matter deeply to the world. How to protect
communications metadata is one of the key open research questions of the
century, and nobody has all the answers. Our best chance at solving it is for
researchers and developers all around the world to team up and work in the
open to build on each other's progress.

Since starting Tor, we've done over 100 Tor talks to university
research groups all around the world, teaching grad students about these
open research problems in the areas of censorship circumvention (which
led to the explosion of pluggable transport ideas), privacy-preserving
measurement, traffic analysis resistance, scalability and performance,
and more.

The result of that effort, and of Tor's success in general, is a flood of
research papers, plus a dozen research labs who regularly have students
who write their theses on Tor. The original Tor design paper from 2004
now has over 4000 citations, and in 2014 Usenix picked that paper out
of all the security papers published in 2004 to win their Test of Time award.

\subsubsection{Tor research resources}

Resources for researchers are collected within the Tor Research
Portal~\cite{research-portal} which is currently undergoing a redesign. This
portal serves as a landing point for anyone interested in doing Tor-related
research. A mock-up of the redesigned portal can be seen in
Figure~\ref{fig:research-portal-mock}.

\begin{figure}
\begin{center}
\fbox{\includegraphics[width=0.8\textwidth]{task8/researchportal.png}}
\end{center}
\caption{Mock-up of redesigned Tor Research Portal.}
\label{fig:research-portal-mock}
\end{figure}

\paragraph{Datasets}

We've been collecting data to learn more about the Tor network: how many relays
and clients there are in the network, what capabilities they have, how fast the
network is, how many clients are connecting via bridges, what traffic exits the
network, etc. We are also developing tools to process these huge data archives
and come up with useful statistics. These datasets are described in more detail
in Section~\ref{sec:public-datasets}.

\paragraph{Measurement and attack tools}

We're building a collection of tools that can be used to measure, analyze, or
perform attacks on Tor. Many research groups need to do similar
measurements (for example, change the Tor design in some way and then see if
communication delays are reduced), and we hope to help everybody standardize on
a few tools and then make them really good. Also, while there are some really
neat Tor attacks that people have published, it can be hard to track down
the code they used in their experiments.

We need defenses too\textemdash not just attacks. Most researchers find it easy
and fun to come up with new attacks on anonymity systems. We've seen this
lately in terms of improved congestion attacks, attacks based on
remotely measuring latency or throughput, and so on. Knowing how things can go
wrong is important, and we recognize that the incentives in academia aren't
aligned with spending energy on designing defenses, but it sure would be great
to get more people working on how to address the attacks.

\subsubsection{Tor research safety board}

The research safety board is a group of researchers who study Tor,
and who want to \emph{minimize privacy risks while fostering a better
understanding of the Tor network and its users}. We aim to accomplish
this goal in three ways:

\begin{itemize}
\item Develop and maintain a set of guidelines that researchers can
use to assess the safety of their Tor research.
\item Give feedback to researchers who use our guidelines to assess
the safety of their planned research.
\item Teach program committees about our guidelines, and encourage
reviewers to consider research safety when reviewing Tor papers.
\end{itemize}

The board has seen early success in three ways. First, it has handled a
dozen or so cases, where research groups wrote their research plans and a
safety self-assessment and submitted them to the board. The board engaged in a
discussion with the researchers to help them flesh out their assessment and to
help them think through their safety plans.

Second, there are a small but growing set of papers out there on
\emph{privacy-preserving measurement}, which expand the toolbox of safe ways to
do network-wide measurements. 

And third, major conferences like PETS and Usenix Security
are starting to ask authors about their safety board interactions when the
paper includes some questionable or concerning methodology, and mainstream
papers are starting to include an ``Ethics of Data Collection'' section
or the equivalent~\cite{deep-corr18}.

To go with these successes, we have three things we want to do in the near term
to position the research safety board for further success. First, we need to
publish the details of more past cases, so future researchers can benefit from
reading them. One of the main goals of the safety board is to provide
historical examples so prospective researchers can get concrete ideas of how to
assess safety. Authors generally agree to publish their interactions,
but only after their paper has gone public---which means we need better ways of
tracking the status of old safety board cases and noticing when we can make
them public.

Second, we need to improve our workflow for noticing and responding to new
cases. In the past we used a shared mailing list, but some people found it hard
to interact with. Now we use our own HotCRP instance, but there are some gaps
in the workflow for noticing and assigning new submissions, and we sometimes
miss them. The fundamental problem here is that we have only busy professors
involved. Maybe we need to expand the set of volunteers to include people who
can better manage submissions and coordinate review.

Finally, we need to do more outreach to program
committees. We've heard anecdotes from several program committee
members who excitedly told us that they asked authors ``what the Tor
safety board thought''---and that's a fine first question, but it's
misunderstanding the intended role of the safety board. We don't want
to be the gatekeepers of Tor research. Ultimately, we want reviewers to
be demanding a discussion of safety in the paper, not a discussion of
the authors' interaction with the safety board.

