\subsubsection{Tor Browser}
\label{browser-censorship}

In many places in the world, censorship of the web is common. Users are
routinely blocked from connecting to websites by ISPs, governments, corporate
firewalls, and public access points such as internet cafes. Websites are shut
down, logs of users' connections are seized, and users can be punished for
their individual browsing history.

Tor Browser is designed to combat this systemic assault on the rights of
individuals. Uniquely among all available web browsers, Tor Browser empowers
millions of users around the world to bypass censorship and read any website
they want, without interference.

Since the project began in 2008, as the Tor Browser Bundle, Tor Browser has
evolved from a bundle of related projects into a clean, streamlined, and usable
web browser. At its core, Tor Browser is a modified version of Mozilla Firefox,
providing more privacy protections than the standard
browser~\cite{tor-browser-design}. Fortunately, with Mozilla's support, many of
the modifications written for Tor Browser are now also integrated into Firefox~\cite{tor-uplift}.

Sometimes censors attempt to prevent users from downloading and installing Tor
Browser by blocking access to the Tor website. Our GetTor service provides a
way for users to get Tor Browser from another site. GetTor was developed by an
intern participating in Google's Summer of Code. Due to resource constraints,
it is not currently maintained and does not automatically check for new
versions of Tor Browser and so sometimes delivers out of date versions. An
auto-update mechanism is planned that will ensure that users who contact the
service will always get the latest version of Tor Browser.

Tor Browser 8.0 provides a new feature called \emph{moat} to make it easy for
a user to request bridge addresses from within the application. Moat presents the user with a CAPTCHA to solve, then Tor Browser receives
additional, unpublished, bridge addresses over a secure encrypted connection.
Tor Browser automatically configures itself to use the new bridge addresses.

The bridges Tor Browser provides use pluggable transports to evade blocking
based on deep packet inspection. The pluggable transports currently used are
obfs4, FTE, meek (through the Microsoft Azure cloud), and obfs3. For
descriptions of the
types of pluggable transports see Section~\ref{pt-types}.

Although using tools to get around censorship currently requires some level of
technical knowledge, we have made improvements to help users connect to
the internet from censored locations. We have been working with local
speakers of non-English languages to find metaphors that can help explain
complex concepts. As a result of this user-focused effort, Tor Browser is now
available in 25 languages. For more information about our international user
testing for Tor Browser 8.0, see Section~\ref{ux-testing-outreach}.

We are doing ongoing work to make it easier for users to bypass censorship. We
are in the planning stages for a much simpler interface to provide fully
automated bridge setup for users behind national firewalls. We expect this work
to make anti-censorship technologies much more widely accessible to users.

Measures within Tor browser that keep people safe are often especially relevant
to users under censorship. Our reproducible build process makes it so that
anyone can (and should!) verify that the software we release is based on the
source code we say it is. An independent party can take our source code and the
specifications for our build environment and build Tor Browser. All of the
resulting packages should be identical, byte for byte, to the software we
release. This is an important way to verify the integrity of our software and
thus increase user safety.

Another feature that enhances user safety is the domain isolation feature (this
is sometimes referred to as ``first party isolation''). Browser tabs that are
connected to one internet domain run in a separate environment from tabs
connected to another domain. Each domain has its own circuit through the Tor
network, and cookies and other trackers from each domain are stored separately
from those of other domains. These separate environments make it difficult for
websites or tracking networks to link activity between domains. This means that
it is safe, for example, to be logged into Facebook in one tab and to post
anonymously to a blog in another tab. This is a Tor feature that Mozilla has
also implemented in Firefox.

\paragraph{Usability testing and global outreach}
\label{ux-testing-outreach}

To make useful software, especially software that aims to keep people safe, it
is essential to understand users' contexts and goals. This is especially true
for tools that require some level of technical knowledge to use successfully,
such as tools for evading censorship.

As part of our global south initiative, members of the Tor user experience and
community teams traveled through India, Uganda, Colombia, and Kenya to run
usability testing. We ran in-person testing of several user experience (UX)
improvements in Tor Browser 8.0 with users of different technical backgrounds.
We conducted more than 45 interviews~\cite{ux-research} that gave us an
understanding of the diverse mental models and technical levels of knowledge of
our users.

We met people like Juana, a coffee farmer in Colombia who is part of a
self-managed group of women coffee growers. The group uses Tor to communicate
securely with one another. We met Jon, an environmental activist and journalist
in Hoima, Uganda who uses Tor to anonymously publish his blog.

We learned about several threat models (Figure~\ref{uganda-threat-model}) of the people
we spoke to in Uganda. Concern about local police retaining hardware devices,
and the practice of the political party currently in power seizing journalists
notes and other records, were common in most of these communities.

\begin{figure}
\begin{center}
\fbox{\includegraphics[width=0.8\textwidth]{task7b/threat-model-uganda-debrief.png}}
\end{center}
\caption{Threat Model.}
\label{uganda-threat-model}
\end{figure}

In addition, this work allowed us to speak with people who use our software
under extreme conditions, such as poor telecommunications infrastructure, very
expensive data packages or very old hardware. This helped us to understand
their context, empathize, and consider solutions that work for them. We believe
that Tor Browser must be usable by people without a technical background, as
well as by advanced users. We want to empower our users through education so
that they can have control of their browsing.

We use an open design process~\cite{opendesignkit}
where developers and designers work together to achieve the best solution. We
use knowledge gained from our research to help us to create the most usable
flow for users.

As part of user education, Tor Browser's on-boarding---a short in-browser
orientation provided when a user first opens the application---aims to teach
users the complex concepts underlying the Tor network using local language
metaphors and simple words.

In many places, Tor Browser is considered to be the highest standard for
protecting people's privacy and helping them evade censorship. However, many
people who learn about Tor express fear, at first, based on stories they have
heard or misconceptions they have. Our on-boarding aims to give our users
confidence by explaining how Tor's privacy and security measures can protect
them. All of the information we give to users is especially critical when
users are in censored environments. They need to understand the trade-offs
and the risks they may run when they decide to opt in or opt out of a feature.

After the user downloads Tor Browser, installs it successfully, and first
starts it, if they are in a censored country, they need to make decisions about using bridges and pluggable transports.
But not all of our users know whether they are in a censored location.
An important subject for future work is exploring the technical options for
detecting whether the location is censored without compromising user safety.
We could probe the internet from Tor Browser to detect whether the user's
connection is being censored and then automatically configure bridges and
pluggable transports. This would certainly improve Tor Browser's usability.
However, such probing may be detectable by the censor and alert them to the
fact that the user has Tor Browser installed, which may not be desirable.
Currently, Tor Browser errs on the side of safety and does not attempt to
establish any connections until the user configures it.

In the future, with the help of OONI (see Section~\ref{censorship-of-tor}) and
Tor Metrics (see Section~\ref{sec:public-datasets}), we should have a good idea
of the state of censorship and network interference for most countries. When we
know this, we can provide users with suggestions about how to set up their Tor
Browser to get around censorship based on their locations.

Tor Browser 8.0 also includes a new circuit display (Figure~\ref{new-circuit-display}),
which shows the relays the current browser tab is using. This visual aid
helps improve user understanding of how Tor sends their traffic through the Tor
network. 

\begin{figure}
\begin{center}
\fbox{\includegraphics[width=0.8\textwidth]{task7b/onion-circuit-display.png}}
\end{center}
\caption{New Circuit Display}
\label{new-circuit-display}
\end{figure}

\subsubsection{Tor Browser for Android}
\label{tb-android}

Historically, Tor Browser has only been developed for desktop computers running
Microsoft Windows, Mac OS X, and various distributions of Linux.  But most
people now use mobile devices, either as their primary means of getting on the
internet, or at least in addition to using desktop computers. In the past, Tor
relied on partner organizations to make mobile browsers that connect to the Tor
network. Recently, however, we've begun making significant progress on Android
support ourselves.

At the beginning of September 2018, we released the first alpha version of Tor
Browser for Android. This work is heavily based on the Guardian Project's
Android app Orfox. Orfox provides a Tor-connected private web browser on
Android, but doesn't come with all of the privacy protections that are in the
desktop version of Tor Browser. The goal of our recent work in this area is to
provide an Android app that gives users a private browser on Android equivalent
to Tor Browser on desktop.

The current alpha version of Tor Browser for Android still requires an
additional app, Orbot, that connects the browser to the Tor network. In the
near future, Tor Browser for Android will include Tor as part of the app,
making it easier to install and use. 

Tor Browser for Android provides an on-boarding experience for new users that
is similar to the one provided for desktop. This helps new users understand how Tor works to protect their privacy.

The current version of the app does not directly support methods for
circumventing censorship\textemdash we rely on Orbot for this, as well. Similar to
Tor Browser on desktop, Orbot supports the meek and obfs4 pluggable transports.
These provide users with some of the best censorship circumvention techniques
available today. 

When a user configures Orbot to use a language that is primarily used in
certain censored countries, it automatically configures and enables the meek
pluggable transport to help the user get around censorship.

The user can always select and configure another bridge from Orbot's home
screen, if necessary. This screen gives users four options:

\begin{itemize}

	\item If a bridge is already configured, then the user can choose to
	directly connect to the Tor network without using a bridge. 

	\item The user can enable one of the built-in obfs4 bridges (called
	\emph{community servers}).

	\item The user can enable one of the built-in meek bridges (called
	\emph{cloud servers}). 

	\item The user can request another bridge from BridgeDB at
	bridges.torproject.org. The user would then need to configure the bridge
	manually using Orbot's Settings menu.
\end{itemize}

In the future, like Tor Browser for desktop, Orbot and Tor Browser for Android
will support \emph{moat} to make requesting and configuring bridges and
new pluggable
transports easy.

When Tor Browser for Android can connect to the Tor network without the
assistance of Orbot, we still envision the two being able to help each other,
perhaps by sharing configuration information. For example, if a user already
has Orbot installed to connect a different app to the Tor network, when they
install Tor Browser for Android, it could automatically configure itself to use
the bridges that Orbot uses.

Our goal is to make Tor Browser for Android support all of our existing methods
to get around censorship. As research and development continue, we will add
new pluggable transports like Snowflake. We hope to learn from the work the
Guardian Project is doing in this area, as described in
Section~\ref{pt:snowflake}.

As we do further work on Tor Browser for Android to include the features that
are already in Tor Browser for desktop, we plan to implement many of the user
interface improvements discussed in Section~\ref{browser-censorship}. But
because mobile displays are very different from desktop browsers, some of
these, such as the circuit display that shows the user which relays the browser
is using, will need to be redesigned.

We also plan to add an indicator that will let users know when they are
connected to a website running an onion service, so they can tell that
their connection is more secure than a connection to the regular web.

One subject of ongoing investigation is browser fingerprinting. The information
the browser exposes to websites can be used to identify either an individual
user or a group of users. Mobile devices have more physical sensors than
stationary computers, and that exposes more potential pieces of information
from which to construct a fingerprint.
